import json


def write_config(config_dict, filename="vaccin.conf"):
    with open(filename, 'w') as outfile:
        json_string = json.dumps(config_dict, indent=2)
        outfile.write(json_string)


def read_config(filename="vaccin.conf"):
    with open(filename, 'r') as infile:
        content = infile.read()

    config = json.loads(content)
    return config

def get_config():
    try:
        raw_config = read_config()
        config = {
            "template": raw_config.get('template', 'vac_intivation.j2'),
            "output_directory": raw_config.get('output_directory', 'invitations'),
            "template_directory": raw_config.get("template_directory", "templates")
        }

    except Exception:
        config = {
                "template": "vac_invitation.j2",
                "output_directory": "text",
                "template_directory": "templates"
            }
    finally:
        return config




if __name__ == "__main__":
    config = get_config()
    print(config)
    write_config(filename="test_config.conf",
                 config_dict=config)