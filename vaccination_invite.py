import sys
import os
import argparse
from jinja2 import Template
from jinja2 import FileSystemLoader, Environment
import config_utils as cu




def print_usage():
    print("usage:", sys.argv[0], "<directory>")


def parse_program_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--template', nargs=1)
    parser.add_argument('--dest', '-d', type=str)
    parser.add_argument('--test', '-t', action='store_true', default=False)
    args = parser.parse_args()
    return args


def get_user_data():
    print("Getting user data")
    return { 'persons': [
        { 'id': 'p1',
          'name': 'Guido van Rossum',
          'email': "guido@python.org"},
        {'id': 'p2',
         'name': 'Raymond Hettinger',
         'email': "raymond@python.org"}
        ]
    }


def write_to_file(content, directory, filename, test=False):
    print("Writing content to ", filename)

    if not os.path.exists(directory):
        os.mkdir(directory)
    total_path = os.path.join(directory, filename)

    if not test:
        with open(total_path, 'w') as outfile:
            outfile.write(content)
    else:
        print("writing file", total_path)


def get_vaccin_data():
    # fixture: TODO: read from json file
    data = { 'vaccins': [
                    { 'name': 'AstraZenica'},
                    { 'name': 'Janssen'},
                    { 'name': 'Spoetnic'},
            ]
    }
    return data


def create_faxination_invites(template_name, output_directory, templates, test=False):
    ''' lijst van email-adressen en naam

    - template mailbody ophalen
    - voor elke persoon:
        render template
        sla het resultaat op in output directory

    '''
    print("create_faxination_invites")

    data = get_user_data()
    data['vaccins'] = get_vaccin_data()['vaccins']
    print(data)
    template = templates.get_template(template_name)

    print("Loop over personen")
    for person in data['persons']:
        template_data = { 'name': person['name'],
                          'vaccines': data['vaccins']
        }

        content = template.render(template_data)
        print(content)
        filename = person['id'] + ".txt"
        write_to_file(content, output_directory, filename, test=test)
        #send_email(address, content)



def main():
    print("Starting fax_invite")

    args = parse_program_arguments()
    print(args)

    config = cu.get_config()

    templates = Environment(loader=FileSystemLoader(config['template_directory']))

    create_faxination_invites(template_name=config['template'],
                              output_directory=config['output_directory'],
                              templates=templates,
                              test=args.test
                              )

    sys.exit(0)
    '''
    except Exception as e:
        print(e)
        sys.exit(1)
    '''

if __name__ == "__main__":
    main()